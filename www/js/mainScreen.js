var APP_STORAGE_KEY = "wld_sp_fiverr_key";
 new WOW().init();
var appState = {
    isInAction : false,
    actionTitle : null
}
var media = null;
var array = ["Glitter",
             "Surprise !",
             "Stampede",
             "Inhale",
             "Peek",
             "Authentic",
             "Limited edition",
             "Jewel",
             "Secret Shhhh!",
             "Lick",
             "Box fresh",
             "Banquet",
             "Slice",
             "Scream",
             "Chinese whispers",
             "Heavenly",
             "Ninja",
             "Shatter",
             "Conundrum",
             "Spell",
             "Overcast",
             "Current",
             "How would Harry Potter solve this challenge?",
             "How would you solve this challenge with just a bag of sweets?",
             "How would a taxi driver solve this challenge?",
             "What invention could help you solve this challenge?",
             "How would Amazon solve this challenge?",
             "How would you use the army to solve this challenge?"];
var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        // Here we register our callbacks for the lifecycle events we care about
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('pause', this.onPause, false);
        document.addEventListener('resume', this.onResume, false);    
    },
    onDeviceReady: function() {

        media = new Media(getMediaURL("spark.mp3"),
                        function(){
                            //onSuccesscallback
                        },
                        function(){
                            //onError callback
                            alert('err')
                    },function(e){

                        if(e == Media.MEDIA_RUNNING || e == Media.MEDIA_STARTING ){
                            navigator.vibrate(1000);
                            $('#randElemHolder').text(pickElementAtRandom()).css({"visibility":"hidden"});
                        }else if(e == Media.MEDIA_STOPPED ){
                            
                            app.animateSparks(false);
                            $('#randElemHolder').text(pickElementAtRandom()).css({"visibility":"visible"});
                        }
                });

        function getMediaURL(s) {
            if(navigator.userAgent.toLowerCase().indexOf("android") != -1) return "/android_asset/www/audio/" + s;
            return s;
        }

        var state = JSON.parse(window.localStorage.getItem(APP_STORAGE_KEY));
        alert(JSON.stringify(window.localStorage.getItem(APP_STORAGE_KEY)));
        if(state.isInAction){
            appState = state;
            $('#view_title').text(state.actionTitle).fadeIn(250);
            $("#user_input").css({"visibility" : "hidden"}).css({"display":"inline-block"});
            $("#instruction_text").hide();
            $('#randElemHolder').addClass("wow fadeInDown").text(pickElementAtRandom()).css({"visibility":"visible"});
        }
        document.addEventListener("backbutton", function(){
            window.cache.clear( function(){
                window.localStorage.setItem(APP_STORAGE_KEY,'{}');
                window.localStorage.clear();
                window.cache.cleartemp();
                navigator.app.exitApp();
            }, function(){alert('failed clearing cache')} );
        }, false);
    },
    onPause: function() {
        window.localStorage.setItem(APP_STORAGE_KEY, JSON.stringify(appState));
    },
    onResume: function(event) {

        var viewTitle = window.localStorage.getItem("viewTitle");
        $('#view_title').text(viewTitle).fadeIn(250);
    },
    spark:function(){

        var value = $("#user_input").val();
        if(!appState.isInAction){
            if(value.trim() == ""){
                $("#user_input").focus().css({"border-color":"red"})
            }else{
                $('#view_title').text(value).fadeIn(250);
                $("#user_input").css({"visibility" : "hidden","display":"inline-block"});
                $("#instruction_text").hide();
                appState.actionTitle = value;
                appState.isInAction = true;
                media.play();
                app.animateSparks(true);
            }
        }else{
            //perform the spark
            media.play();
            app.animateSparks(true);
        }
    },
    redirectTo:function  (des) {
        window.localStorage.setItem(APP_STORAGE_KEY,JSON.stringify(appState));
        if(appState.isInAction){
            window.localStorage.setItem("viewTitle",$('#view_title').text());
        }
            window.location.replace(des);
    },
    animateSparks:function(b){
        if(b){
            $('#sparkHolder .spark_right , .spark_left').animate({'width':  "50%" }, 200,function(){
                $('#sparkHolder').show();
            });
        }else{
            $('#sparkHolder .spark_right , .spark_left').animate({'width': "0%"}, 200,function(){
                $('#sparkHolder').hide();
            });
        }

        for(i=0;i<4;i++) {
            $("#sparkHolder .spark_right , .spark_left").fadeTo(100, 0.2).fadeTo(100, 1.0);
        }
    }
}
    function pickElementAtRandom(){
            return array[Math.floor(Math.random() * array.length) + 1];
    }
app.initialize();
